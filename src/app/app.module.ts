import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { LoginPage } from '../pages/login/login';
import { ProductListPage } from '../pages/product-list/product-list';
import { ProductDetailPage } from '../pages/product-detail/product-detail';
import { MyCartPage } from '../pages/my-cart/my-cart';
import { Cart } from "../providers/cart";

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    ProductListPage,
    ProductDetailPage,
    MyCartPage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    ProductListPage,
    ProductDetailPage,
    MyCartPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}, Cart]
})
export class AppModule {}
